import React from "react";
import {
  View,
  Text,
  Platform,
  Image } from "react-native";
import { createStackNavigator, createAppContainer ,createBottomTabNavigator} from "react-navigation";
import SplashScreen from 'react-native-splash-screen';
// import Image from 'react-native-remote-svg';

import RecipeList from './src/recipeList';
import MyKitchen from './src/myKitchen';
import MyPoint from './src/myPoints';

import Recipes from './src/recipes';
import RecipeDetails from './src/recipeDetails'
import AllRecipes from './src/allRecipes'
import FavouriteRecipe from './src/favouriteRecipe'

import * as APIHelper from './helper/apiHelper';

import * as Constants from './common/Constants'


const RecipeListStack = createStackNavigator(
  {
    RecipeList: { screen: RecipeList } 
  },
  {
    headerMode : 'screen'
  }
);
const RecipeListAppContainer = createAppContainer(RecipeListStack);

const MyKitchenStack = createStackNavigator(
  {
    MyKitchen: { screen: MyKitchen } ,
    // Usual: {screen : Usual},
    // Refrigerator: {screen : Refrigerator},
    // Cabinet: {screen : Cabinet},
    // Freezer: {screen : Freezer}
  },
  {
    headerMode : 'screen',
    initialRouteName : "MyKitchen"
  }
);
const MyKitchenAppContainer = createAppContainer(MyKitchenStack);

const RecipesAppStack = createStackNavigator(
  {
    Recipes: { screen: Recipes, params: {hideSearchButton: false , hideFilterButton: false, isFilterBadge: false} },
    AllRecipes: {screen: AllRecipes},
    FavouriteRecipe: {screen: FavouriteRecipe},
    RecipeDetails : {screen : RecipeDetails}
    
  },
  {
    headerMode : 'screen',
    initialRouteName : "Recipes"
  }
);
const RecipesAppContainer = createAppContainer(RecipesAppStack);

const MyPointStack = createStackNavigator(
  {
    MyPoint: { screen: MyPoint } 
  },
  {
    headerMode : 'screen'
  }
);
// const MyPointAppContainer = createAppContainer(MyPointStack);

const BottomTabNavigator = createBottomTabNavigator(
  {
    RecipeList: { screen: RecipeListAppContainer , navigationOptions: {
      tabBarLabel: "Liste",
      tabBarIcon: ({ tintColor, focused }) => (
        <Image source = {Constants.LIST_ICON} style = {{height : 25,width : 25 ,tintColor : focused ? '#F35353' : '#71777F'}}/>
      ),
    } },
    Recipes: { screen: RecipesAppContainer , navigationOptions : {
      tabBarLabel: "Oppskrifter",
      tabBarIcon: ({ tintColor, focused }) => (
        <Image source = { Constants.RECIPE_ICON } style = {{height : 25,width : 25 ,tintColor : focused ? '#F35353' : '#71777F' }}/>
      ),
    }},
    MyKitchen: { screen: MyKitchenAppContainer , navigationOptions : {
      tabBarLabel: "Mitt Kjøkkenet",
      tabBarIcon: ({ tintColor, focused }) => (
        <Image source = { Constants.MYKITCHEN_ICON} style = {{height : 25,width : 25,tintColor : focused ? '#F35353' : '#71777F' }}/>
      ),
    } },
  },
  {
    tabBarOptions: {
      // activeBackgroundColor : 'rgb(220,20,60)', 
      activeTintColor : '#F35353',
      inactiveTintColor: '#71777F',
      labelStyle: {
        fontSize: 12,
        alignSelf: 'center',
        fontFamily : Constants.RobotoRegular,
      },
      allowFontScaling: false,      
    },
    initialRouteName : 'Recipes',
  }
);

// MyPoint: { screen: MyPointAppContainer , navigationOptions : {
//   tabBarLabel: <Text allowFontScaling={false} style={{color:Constants.BOTTOM_TABBAR_FONT_COLOR,textAlign:'center',fontSize:Constants.BOTTOM_TABBAR_FONT_SIZE,marginBottom:Constants.MARGIN_BOTTOM_TABBAR}}>{"Mine Poeng"}</Text>,
// } }

const MainTabNavigator = createAppContainer(BottomTabNavigator);


export default class App extends React.Component {
  
  componentWillMount() {
    console.disableYellowBox = true
    this.getMeasurements();
  }

  getMeasurements() {
    let url = APIHelper.baseURL + APIHelper.getMeasurements;
    APIHelper.getRequest(url, {
      'Content-Type' : 'application/json',
      'ApiKey' : '0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654'
    }, (error, response) => {
      if(error === null && response === null) {

      } else if(error != null) {
        alert('Error' + JSON.stringify(error));
      } else {
        this.handleMeasurementResponse(response);
      }
    })
  }

  handleMeasurementResponse = (response) => {
    if(response.Status == true) {
      window.measurementsArray = response.Data;
    }
  }

  componentDidMount() {
    if(Platform.OS == "ios"){
      SplashScreen.hide();
    }
    
  }
  render() {
    return (
      <View style = {{height :'100%',width : '100%'}}>
         <MainTabNavigator/>
      </View>
       
    );
  }
}