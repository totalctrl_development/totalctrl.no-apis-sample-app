import { 
    NetInfo,
    Alert
 } from "react-native";
 import * as Constants from '../common/Constants';


export const baseURL                =   'http://dev.totalctrl.no/recipes/api/v1/'

export const getRecipe              =   'recipes/'
export const addFavourite           =   'stores/1/favourites/'
export const getFavouriteRecipe     =   'stores/1/favourites/?UserId=1'
export const removeFavourite        =   'stores/1/favourites/'
export const getCategory            =   'categories/'
export const getMeasurements        =   'measurements/'

function getRequest(url, headers, callback) {
    NetInfo.isConnected.fetch().then(isConnected =>{
        if(isConnected) {
            fetch(url, {
                method: 'GET',
                headers: headers
            })
            .then((response) => response.json())
            .then((responseJson) => {
                callback(null, responseJson)
            })
            .catch((error) => {
                callback(error, null)
            });
        }
        else {
            Alert.alert(Constants.NETWORK_ALERT_TITLE, Constants.NETWORK_ALERT_MESSAGE)
            callback(null, null)
            return
        }
    })
}

function addFavouriteRequest(url, headers,body,callback) {
    NetInfo.isConnected.fetch().then(isConnected =>{
        if(isConnected) {
            fetch(url, {
                method: 'POST',
                headers: headers,
                body : JSON.stringify(body)
            })
            .then((response) => response.json())
            .then((responseJson) => {
                callback(null, responseJson)
            })
            .catch((error) => {
                callback(error, null)
            });
        }
        else {
            Alert.alert(Constants.NETWORK_ALERT_TITLE, Constants.NETWORK_ALERT_MESSAGE)
            callback(null, null)
            return
        }
    })
}

export {
    getRequest,
    addFavouriteRequest
}