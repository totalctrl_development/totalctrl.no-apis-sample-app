import { Dimensions } from 'react-native';
// import * as FontFamily from '../assets/fonts'
export  const deviceHeight = Dimensions.get('window').height;
export  const deviceWidth = Dimensions.get('window').width;


export  const APIKEY = '0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654';
 

//Alert message

export const NETWORK_ALERT_TITLE            =      'Error'
export const NETWORK_ALERT_MESSAGE          =      'Network request failed'

//Font Size 

export const HEADER_FONT_SIZE                =    18
export const BOTTOM_TABBAR_FONT_SIZE         =    15

// font color 

export const BOTTOM_TABBAR_FONT_COLOR        =    'black'
export const ACTIVE_TINT_COLOR               =    'rgba(253,221,221,1) '
export const RECIPE_NAME_COLOR               =    'rgba(28,40,51,1)'
export const RECIPE_AUTHOR_NAME_COLOR        =    'rgba(243,83,83,1)'
export const RECIPE_DURATION_COLOR           =    'rgba(113,119,127,1)'


// margin Bototm for tabbar text 

export const MARGIN_BOTTOM_TABBAR            =    12.5


export const LIST_ICON = require('../res/TabBar/ic_list.png');
export const RECIPE_ICON = require('../res/TabBar/ic_recipe.png');
export const MYKITCHEN_ICON = require('../res/TabBar/ic_kitchen.png');
export const HEADER_SEARCH_ICON = require('../res/HeaderIcon/ic_search.png');
export const HEADER_FILTER_ICON = require('../res/HeaderIcon/ic_filter.png');
export const HEADER_BACK_ICON = require('../res/HeaderIcon/ic_app_bar_back.png');

export const MYKITCHEN_ACTIVE_ICON = require('../res/TabBar/ic_kitchen.png');
export const LIST_ACTIVE_ICON = require('../res/TabBar/ic_list.png');
export const RECIPE_ACTIVE_ICON = require('../res/TabBar/ic_recipe.png');

export const IC_FAVORITE_DESELECTED = require('../res/ic_favorite_deselected.png');
export const IC_FAVORITE_SELECTED = require('../res/ic_favorite_selected.png');
export const IC_NO_FAVOURITE_RECIPE_FOUND = require('../res/ic_favorite_favouriteRecipe.png');

export const IC_DIFFICULTY  = require('../res/RecipeDetail/ic_difficulty.png')
export const IC_DURATION  = require('../res/RecipeDetail/ic_duration.png')
export const IC_PORTIONS  = require('../res/RecipeDetail/ic_portions.png')
export const IC_CLOSE  = require('../res/ic_close.png')

// Font Family
export const RobotoRegular  = 'Roboto-Regular'
export const RobotoBlack    = 'Roboto-Black'
export const RobotoBlackItalic    = 'Roboto-BlackItalic'
export const RobotoBold           = 'Roboto-Roboto-Bold'
export const RobotoBoldItalic    = 'Roboto-BoldItalic'
export const RobotoItalic    = 'Roboto-Italic'
export const RobotoLight    = 'Roboto-Light'
export const RobotoLightItalic    = 'Roboto-LightItalic'
export const RobotoMedium    = 'Roboto-Medium'
export const RobotoMediumItalica    = 'Roboto-MediumItalic'
export const RobotoThin    = 'Roboto-Thin'
export const RobotoThinItalic    = 'Roboto-ThinItalic'



// export const HEADER_FILTER_ICON = require('../res/HeaderIcon/ic_filter.svg');