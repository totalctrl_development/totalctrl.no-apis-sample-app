import React from "react";
import {
    View,
    Text,
    StyleSheet,
    Dimensions
} from 'react-native'
import * as Constants from '../common/Constants'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Usual from './usual'
import Refrigerator from './refrigerator'


const FirstRoute = () => (
  <Usual/>
);
const SecondRoute = () => (
    <View style={{ backgroundColor: 'yellow',height:"100%",width:'100%' }} />
  );
const Thirdoute = () => (
  <View style={{ backgroundColor: 'green',height:"100%",width:'100%' }} />
);
const Fourthoute = () => (
  <View style={{ backgroundColor: 'blue',height:"100%",width:'100%' }} />
);

export default class MyKitchen extends React.Component {
   
    static navigationOptions = ({ navigation }) => ({
        title: "Mitt Kjøkkenet",
        headerTintColor: 'black',
        headerTitleStyle: {
          alignSelf:'center',
          flex: 1,
          justifyContent: 'center',
          textAlign: 'center',
          fontSize:Constants.HEADER_FONT_SIZE,
          fontFamily: Constants.RobotoMedium,
          fontWeight:'500',
          allowFontScaling : false,
          color  : 'rgba(28,40,51,1)'
      },
    });

    state={
      index: 0,

      routes: [
          { key: 'usual', title: 'Vanlig',navigation: this.props.navigation },
          { key: 'refrigerator', title: 'Kjøleskap',navigation: this.props.navigation },
          { key: 'cabinet', title: 'Skap',navigation: this.props.navigation },
          { key: 'freezer', title: 'Fryseboks' ,navigation: this.props.navigation},
        ],
    }

    render(){

        return (
          <View>
            
          </View>
      //     <TabView 
      //     navigationState={this.state}        
      //     renderScene={SceneMap({
      //       usual: FirstRoute,
      //       refrigerator: SecondRoute,
      //       cabinet: Thirdoute,
      //       freezer: Fourthoute
      //     })}
      //     onIndexChange={index => this.setState({ index })}        
      //     initialLayout={{ width: Dimensions.get('window').width }}
      //     renderTabBar = { props =>
      //     <TabBar
      //       {...props}
      //       indicatorStyle = {{ backgroundColor:  Constants.ACTIVE_TINT_COLOR, height: 3 }}
      //       labelStyle = {{ color: 'black', fontSize: 16,width:"100%",textAlign:'center'}}
      //       style = {{ backgroundColor: 'white'}}
      //       getLabelText={({ route }) => route.title}  
      //     />
      //   }
      // />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1 
    }
})
