import React from "react";
import {
    View,
    Text,
    StyleSheet
} from 'react-native'
import * as  Constants from '../common/Constants'



export default class Cabinet extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: "Skap",
        headerTintColor: 'black',
        headerTitleStyle: {
            alignSelf:'center',
            flex: 1,
            justifyContent: 'center',
            textAlign: 'center',
            fontSize:Constants.HEADER_FONT_SIZE,
            fontWeight:'bold',
        },
        headerRight : <View style ={styles.headeright}>
        <Text style ={{fontWeight : '500'}}>{"Rydd liste"}</Text>
        </View>
    });
    render(){
        return (
            <View styles = {styles.container}>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1 
    },
    headeright: {
        width : '100%',
        height : '100%',
        justifyContent : 'center',
        marginRight : 10
    },
})
