import React from "react";
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    NetInfo,
    ActivityIndicator,
    Alert,
    TouchableOpacity,
    Modal,
    Image,
    ScrollView,
    StatusBar,
    Platform
} from "react-native";

// import Image from 'react-native-remote-svg';
import PropTypes from 'prop-types';
import  { SearchBar }  from 'react-native-elements'; 
import { TagSelect } from 'react-native-tag-select';

import * as  Constants from '../common/Constants'
import * as APIHelper from '../helper/apiHelper' 

import EventEmitter from "react-native-eventemitter";
// import { TouchableOpacity } from "react-native-gesture-handler";

const limit = 20
window.filterArray = [];
window.selectedCategoryArr = [];
window.selectedDifficultyArr = [];

export default class AllRecipes extends React.Component {
   
    constructor(props) {
        super(props);
        this.state = {
            recipesList : [],
            offset : 0 ,
            isDataAvailable : true,
            isLoading : false,
            searchRecipe : '',
            searchListArr : [],
            recipesFound  : true,
            favouriteRecipe : '',
            modalVisible : false,
            searchBarVisible : false,   
            searchProgress : true,
            categoryArray: [],
            difficultyArray: [
                {id: 'a1', label: 'Lett',isSelect : false},
                {id: 'a2', label: 'Middels',isSelect : false},
                {id: 'a3', label: 'Vanskelig',isSelect : false},
            ],
            selectedArray : [],
            selectedCategoryArray: [],
            selectedCategoryId: '',
            selectedDifficultyArray: [],
            selectedDifficultyId: '',
            filteredArray: [],
            isFilter: false,
            isFooterLoading : false
        }
    }

    search = null;

    setModalVisible (visible) {
        var categoryIdStr = visible ? '' : this.state.selectedCategoryArray.toString();
        var difficultyIdStr = visible ? '' : this.state.selectedDifficultyArray.toString();

        window.filterArray = this.state.selectedArray;
        window.selectedCategoryArr = this.state.selectedCategoryArray;
        window.selectedDifficultyArr = this.state.selectedDifficultyArray;

        this.setState({ 
            modalVisible : visible,
            selectedCategoryId: categoryIdStr,
            selectedDifficultyId: difficultyIdStr,
        })
        if (categoryIdStr != this.state.selectedCategoryId || difficultyIdStr != this.state.selectedDifficultyId) {
            this.setState({
                searchProgress: true,
                isDataAvailable: true,
                isLoading: true,
                recipesList: [],
                offset: 0,
                isFilter: true
            })
            setTimeout(() => { 
                this._getRecipeList();
            }, 2000);
        }
        else {
            this._getRecipeList()
            if(this.state.selectedArray.length == 0) {
                this.setState({
                    isFilter: false 
                })
            }            
        }
    }
    
    componentWillMount() {
        EventEmitter.on("ModalVisible", (value)=>{
           this.setModalVisible(true)
        });  

        // EventEmitter.on("changeTabIndex", (value) => {
        //     window.filterArray = this.state.selectedArray;
        //     window.selectedCategoryArr = this.state.selectedCategoryArray;
        //     window.selectedDifficultyArr = this.state.selectedDifficultyArray;
        // })

        this._getCategoryList();      
    }

    componentDidMount() {
        
        this.setState({
            modalVisible: false,
            selectedArray: window.filterArray,
            selectedCategoryId: window.selectedCategoryArr.toString(),
            selectedDifficultyId: window.selectedDifficultyArr.toString(),
            isFilter: window.filterArray.length > 0 ? true : false,
        })   
        
        if(window.filterArray.length > 0) {               
            this._getCategoryList();
            this._getDifficultyArray();
            this.setState({
                searchProgress: true,
                isDataAvailable: true,
                isLoading: true,
                recipesList: [],
                offset: 0,
            })
            setTimeout(() => { 
                this._getRecipeList();
            }, 200);
        } else {

            this._getRecipeList();
        }
        EventEmitter.on("onTapCancel", (value)=>{
            this.setState({
                searchRecipe : '',
                searchListArr : [],
                recipesFound : true,
                searchBarVisible  :false
            })
            setTimeout(() => {
                this._getRecipeList()
            }, 100);    
        });
        EventEmitter.on("SearchBar", (value)=>{
            this.setState({
                searchBarVisible : true
            })
        });

    }

    _getDifficultyArray() {

        for(var i=0; i<this.state.difficultyArray.length; i++) {
            let dataObj = this.state.difficultyArray[i];
            var isSelected = false;
            if(window.filterArray.length > 0) {
                for(var j=0; j<window.filterArray.length; j++) {
                    let filterData = window.filterArray[j];
                    if(filterData.id === dataObj.id) {
                        isSelected = true;
                    }
                }
                dataObj['isSelect'] = isSelected;
            }
        }
    }

    _getCategoryList() {

        let url = APIHelper.baseURL + APIHelper.getCategory;
        APIHelper.getRequest(url, {
            'Content-Type' : 'application/json',
            'ApiKey' : '0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654'
        }, (error, response) => {
            if(error === null && response === null) {

            } else if(error != null) {
                // alert('Error' + JSON.stringify(error));
            } else {
                this.handleCategoryResponse(response);
            }
        })
    }

    handleCategoryResponse = (response) => {

        if (response.Status === true) {

            let dataArray = response.Data;
            var categoryDataArray = [];
                        
            for(var i=0; i<dataArray.length; i++) {
                let dataObj = dataArray[i];
                let categoryDataObj = {};
                categoryDataObj['id'] = dataObj.Id;
                categoryDataObj['label'] = dataObj.Name;
                var isSelected = false;
                if(this.state.selectedArray.length > 0) {
                    for(var j=0; j<this.state.selectedArray.length; j++) {
                        let filterData = this.state.selectedArray[j];
                        if(filterData.id === dataObj.Id) {
                            isSelected = true;
                            break;
                        }
                    }
                }
                categoryDataObj['isSelect'] = isSelected;
                categoryDataArray.push(categoryDataObj);
            }
            this.setState({
                categoryArray: categoryDataArray
            })
        }
        else {
          alert(response.Message)
        }
    }

    _getRecipeList() {     
        
        let offset = this.state.offset
        let searchListUrl = APIHelper.baseURL + APIHelper.getRecipe + '?CategoryId=&Language=&Difficulty=&Name=' +this.state.searchRecipe + '&Offset=' + offset + '&Limit=' + limit + '&Portions=1';
        let recipeListUrl = APIHelper.baseURL + APIHelper.getRecipe + '?CategoryId='+ this.state.selectedCategoryId +'&Language=&Difficulty='+ this.state.selectedDifficultyId +'&Name=&Offset=' + offset + '&Limit=' + limit + '&Portions=1';
        var url = this.state.searchRecipe == '' ? recipeListUrl : searchListUrl;
        console.log('URL',url)
        this.setState({
            isLoading : true
        })
     
        APIHelper.getRequest(url, {
            'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
            "Content-Type": "application/json"

        }, (error, response) => {
            if(error === null & response === null) {
                this.setState({
                    isLoading : false,
                    searchProgress : false
                })
            }
            else if(error != null) {
                this.setState({
                    isLoading : false,
                    searchProgress : false
                })
                // alert('Error ' + JSON.stringify(error));
            }
            else {
                this.getRecipeListesponse(response);
            }
        })
    }

    getRecipeListesponse = (response) => {
        this.setState({
            isLoading : false,
            searchProgress : false,
            isFooterLoading : false            
        })
        if(response.Status == true) {
            this.setState({recipesFound : true})
            // check data length if grater zero then assign data to recipeList
         
            if(response.Data.length > 0) {
                if(this.state.searchRecipe == '') {
                    this.setState({
                        recipesList: [...this.state.recipesList, ...response.Data],
                        isDataAvailable: (response.Data.length < limit ) ? false : true,
                        searchListArr : []
                    })
                    this.forceUpdate();
                }
                else {
                    this.setState({
                        searchListArr  : response.Data,
                        isDataAvailable: (response.Data.length < limit ) ? false : true,
                    })
                    this.forceUpdate();
                }
                
            }
            else {
                this.setState({
                    isDataAvailable : false,
                    isFooterLoading : false
                })
            }
        }
        else {
            this.setState({
                recipesFound : false,
                isFooterLoading : false,
            })
            //  alert(response.Message)
        }
    }

    addFavourite = (itemObj,index) => {
        var itemIndex  =  index
        var recipeId = itemObj.Id
        let url = itemObj.IsFavorites ? APIHelper.baseURL + APIHelper.removeFavourite :APIHelper.baseURL + APIHelper.addFavourite 
        NetInfo.isConnected.fetch().then(isConnected =>{
            if(isConnected) {
                if(itemObj.IsFavorites) {
                    fetch(url,{
                        method: 'DELETE',
                        headers: {
                            'Accept': 'application/json',
                            'Content-type': 'application/json',
                            'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
                        },
                        body: JSON.stringify({
                            UserId : 1,
                            RecipeId : recipeId
                        })
                    })
                        .then((response) => response.json())
                        // .then(json => this._handleResponse(json.response))
                        .then((responseJson) => {
                            this.removeFavouriteResponse(responseJson,recipeId,itemIndex);
                        })
                        .catch((error) => {
                            console.error(error);
                        });
                }
                else {
                    fetch(url,{
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-type': 'application/json',
                            'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
                        },
                        body: JSON.stringify({
                            UserId : 1,
                            RecipeId : recipeId
                        })
                        })
                        .then((response) => response.json())
                        // .then(json => this._handleResponse(json.response))
                        .then((responseJson) => {
                            this.getAddFavouriteResponse(responseJson,recipeId,itemIndex);
                        })
                        .catch((error) => {
                            console.error(error);
                        });
                }
            }
            else{
                Alert.alert(Constants.NETWORK_ALERT_TITLE, Constants.NETWORK_ALERT_MESSAGE)
                return
            }
        })
       
       
    }

    removeFavouriteResponse = (response,recipeId,itemIndex) => {
        var favouriterecipeId = recipeId
        var index = itemIndex
        if(response.Status == true) { 
            let recipeObj = this.state.recipesList[index]
            recipeObj.IsFavorites = 0 
            this.setState({
                recipeList:recipeObj 
            })        
        }
        else {
            console.log('recipe  favourite' + favouriterecipeId) 
        }
    }
    getAddFavouriteResponse = (response,recipeId,itemIndex) => {
        var favouriterecipeId = recipeId
        var index = itemIndex
        if(response.Status == true) { 
            let recipeObj = this.state.recipesList[index]
            recipeObj.IsFavorites = 1 
            this.setState({
                recipeList:recipeObj 
            })        
        }
        else {
            console.log('recipe already favourite' + favouriterecipeId) 
        }
    }

    onChangeSearchText = (text) =>{
        if(text === null){
            this.setState({
                searchRecipe : '',
                searchListArr : [],
                searchProgress : true,
                isLoading : true
            })
            
            setTimeout(() => {
                this._getRecipeList()
            }, 100);
        }
        else {
            this.setState({
                searchRecipe : text,
                searchProgress : true,
                searchProgress : true,
                isLoading : true
            })
            
            setTimeout(() => {
                this._getRecipeList()
            }, 300);
        }
      
    }

    onTapCancelSearchText =() =>{
        this.setState({
            searchBarVisible : false,
            searchRecipe : '',
            searchListArr : []
        })
        setTimeout(() => {
            this._getRecipeList()
        }, 300);
       EventEmitter.emit("onTapCancel")
    }

    onTapClearText() {
        this.search.focus();
    }

    onPressCategory = (item) => {
        // let data = this.state.categoryArray[item.id - 1]
        if(window.filterArray.length > 0) {
            for(var i=0; i<this.state.selectedArray.length; i++) {
                let categoryObj = this.state.selectedArray[i];
                if(item.id === categoryObj.id) {
                    this.state.selectedArray.splice(i,1);
                }
            }
        }
        this.state.categoryArray[item.id - 1]['isSelect'] = !this.state.categoryArray[item.id - 1]['isSelect'];
        // data['isSelect'] = !data.isSelect
        // this.setState({
        //     categoryArray : this.state.categoryArray
        // })
       
        this.refreshSelectedTags(false);
    }

    onPressDifficulty = (item) => {
        let index = this.state.difficultyArray.indexOf(item);
        // data1['isSelect'] = !data1.isSelect
        if(window.filterArray.length > 0) {
            if(window.filterArray.length > 0) {
                for(var i=0; i<this.state.selectedArray.length; i++) {
                    let categoryObj = this.state.selectedArray[i];
                    if(item.id === categoryObj.id) {
                        this.state.selectedArray.splice(i,1);
                    }
                }
            }
        }
        this.state.difficultyArray[index]['isSelect'] = !this.state.difficultyArray[index]['isSelect'];
        // this.setState({
        //     difficultyArray : this.state.difficultyArray
        // })
        var isRefreshTags = false
        this.refreshSelectedTags(isRefreshTags);
    }

    onSelectedItemPress = (item) => {  
        
        this.setState({
            searchProgress : true
        })         
        var index = -1;
        var index1 = -1;
        if(window.filterArray.length > 0) {
            for(var i=0; i<this.state.categoryArray.length; i++) {
                let categoryObj = this.state.categoryArray[i];
                if(item.id === categoryObj.id) {
                    this.state.selectedArray.splice(this.state.selectedArray.indexOf(item),1);
                    index = i;
                }
            }
            for(var i=0; i<this.state.difficultyArray.length; i++) {
                let difficultyObj = this.state.difficultyArray[i];
                if(item.id === difficultyObj.id) {
                    this.state.selectedArray.splice(this.state.selectedArray.indexOf(item),1);
                    index1 = i;
                }
            }
        } else {
            index = this.state.categoryArray.indexOf(item)
            index1 = this.state.difficultyArray.indexOf(item)
        }
        if(index > -1){
            let dataObj = this.state.categoryArray[index];
            dataObj['isSelect'] = !dataObj.isSelect
            this.state.selectedCategoryArray.splice(this.state.selectedCategoryArray.indexOf(item.id), 1);
            this.setState({
                categoryArray : this.state.categoryArray,
            })
        }
        if(index1 > -1){
            let dataObj = this.state.difficultyArray[index1];
            dataObj['isSelect'] = !dataObj.isSelect
            this.state.selectedDifficultyArray.splice(this.state.selectedDifficultyArray.indexOf(index1), 1);
            this.setState({
                difficultyArray : this.state.difficultyArray,
            })
        }
        console.log("window array " , JSON.stringify(window.filterArray))
        this.refreshSelectedTags(true);
      
    }

    refreshSelectedTags(isRefresh) {
        for(let i = 0; i<this.state.categoryArray.length;i++){
            let dataObj = this.state.categoryArray[i];
            if(dataObj.isSelect) {
                if(!this.state.selectedArray.includes(dataObj)) {
                    this.state.selectedArray.push(dataObj);
                    this.state.selectedCategoryArray.push(dataObj.id);
                }
            }
            else {
                if(this.state.selectedArray.includes(dataObj)) {
                    let index = this.state.selectedArray.indexOf(dataObj)
                    this.state.selectedArray.splice(index,1); 
                    this.state.selectedCategoryArray.splice(this.state.selectedCategoryArray.indexOf(dataObj.id), 1)              
                }
            }
        }
        for(let i = 0; i<this.state.difficultyArray.length;i++){
            let dataObj = this.state.difficultyArray[i];
            if(dataObj.isSelect) {
                if(!this.state.selectedArray.includes(dataObj)) {
                    this.state.selectedDifficultyArray.push(i);
                    this.state.selectedArray.push(dataObj);
                }
            }
            else {
                if(this.state.selectedArray.includes(dataObj)) {
                    let index = this.state.selectedArray.indexOf(dataObj)
                    this.state.selectedArray.splice(index,1);   
                    this.state.selectedDifficultyArray.splice(this.state.selectedDifficultyArray.indexOf(i), 1);
                }
            }
        }
        if(isRefresh) {

            if(this.state.selectedArray.length > 0) {
                var newArray = [];
                var arr = this.state.selectedArray.filter(function(object) {
                    if (newArray.indexOf(object.id) == -1) {
                        newArray.push(object.id);
                        return true;
                    }
                    return false;
                });
                var categoryIdStr =  this.state.selectedCategoryArray.toString();
                var difficultyIdStr = this.state.selectedDifficultyArray.toString();
        
                this.setState({ 
                    selectedArray: arr,
                    selectedCategoryId: categoryIdStr,
                    selectedDifficultyId: difficultyIdStr,
                });

                if (categoryIdStr != this.state.selectedCategoryId || difficultyIdStr != this.state.selectedDifficultyId) {
                    this.setState({
                        searchProgress: true,
                        isDataAvailable: true,
                        isLoading: true,
                        recipesList: [],
                        offset: 0,
                        isFilter: true
                    })
                    setTimeout(() => { 
                        this._getRecipeList();
                    }, 200);
                }
            } else {
                if (this.state.isFilter) {
                    this.setState({
                        selectedCategoryId: '',
                        selectedDifficultyId: '',
                        searchProgress: true,
                        isDataAvailable: true,
                        isLoading: true,
                        recipesList: [],
                        offset: 0,
                        isFilter: false
                    })
                    window.filterArray = window.selectedCategoryArr = window.selectedDifficultyArr = [];
                    setTimeout(() => {
                        this._getRecipeList();
                    }, 50);
                }            
            }
        }
        else {
            if(this.state.selectedArray.length > 0) {
                var newArray = [];
                var arr = this.state.selectedArray.filter(function(object) {
                    if (newArray.indexOf(object.id) == -1) {
                        newArray.push(object.id);
                        return true;
                    }
                    return false;
                });
                this.setState({ 
                    selectedArray: arr,
                });
            }
        }
        console.log("window array for refresh tag " , JSON.stringify(window.filterArray))
    }

    resetTags = () => {           
        window.filterArray = [];
        window.selectedCategoryArr = [];
        window.selectedDifficultyArr = [];
        this.state.categoryArray.forEach((item) => item.isSelect = false);
        this.state.difficultyArray.forEach((item) => item.isSelect = false);
        
        this.setState({            
            selectedArray : [],
            selectedCategoryArray: [],
            selectedCategoryId: '',
            selectedDifficultyArray: [],
            selectedDifficultyId: '',
            filteredArray: [],
        })
        setTimeout(() => {
            const value = {};
            this.categoryTags.setState({ value });
            this.difficultyTags.setState({ value });
            this.refreshSelectedTags(false);
        }, 200);
      
    }

    _renderItem = (item) => {
        let itemObj = item.item
        var difficulty=  itemObj.Difficulty == 0 ? "Lett" : itemObj.Difficulty == 1 ? "Middels" : "Vanskelig"
        var favouriteImage = itemObj.IsFavorites ? <Image style = {styles.favouriteImage}  source = {Constants.IC_FAVORITE_SELECTED} />: itemObj.Id == this.state.favouriteRecipe ? <Image style = {styles.favouriteImage}  source = {Constants.IC_FAVORITE_SELECTED} /> : <Image style = {styles.favouriteImage}  source = {Constants.IC_FAVORITE_DESELECTED} />
        var RecipeImage =  itemObj.Image == null ? <View style = {styles.recipeImage}></View> : <Image style = {styles.recipeImage}  source = {{uri :itemObj.Image}} />
        let hours =parseInt(itemObj.Duration) / 60
            let minutes =parseInt(itemObj.Duration) % 60
            var duration = ""
            if (parseInt(hours) > 0) {
                if (minutes > 0) {
                    duration = parseInt(hours) + " h" + parseInt(minutes) + ' min'    
                }
                else {
                    duration = parseInt(hours) + " h"                              
                }
            }
            else {
                duration = parseInt(minutes) + ' min'    
            }       
        return (
            <TouchableOpacity onPress = { () => {this.onTapRecipeCell(item)}} activeOpacity = {0.7}>

                <View style ={styles.itemContainer}>
                    <View style = {styles.recipeItem}>
                        {RecipeImage}
                        <Text style={styles.recipeName} allowFontScaling = {false} >{ itemObj.Name }</Text>
                        <Text style={styles.authorName}>{ itemObj.OwnerName }</Text>
                        <View style={styles.durationView}>
                            <Text style = {styles.durationContent} allowFontScaling = {false} >{duration}</Text>
                            <View style = {{height : '90%',width:2,backgroundColor:'rgba(113,119,127,1)',margin : 10}}></View>
                            <Text style = {styles.durationContent} allowFontScaling = {false}>{itemObj.Portions + " porsjoner"}</Text>
                            <View style = {{height : '90%',width:2,margin : 10,backgroundColor:'rgba(113,119,127,1)'}}></View>
                            <Text style = {styles.durationContent} allowFontScaling = {false}>{difficulty}</Text>
                        </View>
                    </View> 
                    <TouchableOpacity onPress = {() => {this.addFavourite(itemObj,item.index)}} style = {styles.favouriteTouchable} activeOpacity = {0.7}>
                        {favouriteImage}
                    </TouchableOpacity>        
                </View>
            </TouchableOpacity>
           
        )    
    }

    renderFooter = () => {
       
        var progressView = <View/> 

        if(this.state.isFooterLoading) {
            progressView = 
            <View style ={styles.renderFooter}>
                <ActivityIndicator style ={styles.loading} color = 'red' size ='large'/>
            </View>    
        }
        return (
            <View>
                {progressView}
            </View> 
        )
    }

    renderSeparator () {

        return (
            <View style ={{height :0 ,width :'100%',backgroundColor :'gray'}}> 
            </View>
        )
    }

    handleLoadMore = () => {
        
        // handle load more data incerement offsetvalue 
        if(this.state.isDataAvailable && !this.state.isLoading) {
            this.setState({
                offset : this.state.offset + limit,
                isFooterLoading : true
                }, ()=>{
                    this._getRecipeList();
            })
        }
    }

    onTapRecipeCell = (item) => {
        // alert(JSON.stringify(item.item.Id))
        let url =  APIHelper.baseURL + APIHelper.getRecipe + item.item.Id            
        APIHelper.getRequest(url, {
            'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
            "Content-Type": "application/json"
        }, (error, response) => {
            if(error === null & response === null) {
                this.setState({
                    isLoading : false
                })
            }
            else if(error != null) {
                this.setState({
                    isLoading : false
                })
                // alert('Error ' + JSON.stringify(error));
            }
            else {
                this.getRecipeDetailResponse(response,item);
            }
        })    

        // this.props.onReceipeTap(item);
        // this.props.navigation.navigate('RecipeDetails', {recipeDetail : item.item , title : item.item.Name , isFavourite  : item.item.IsFavorites});
    }

    getRecipeDetailResponse = (response,item) => {
       
        if(response.Status == true) { 
            this.props.navigation.navigate('RecipeDetails', {title : response.Data.RecipeDetails.Title, isFavourite  : item.item.IsFavorites, ownerName : item.item.OwnerName ,categoryName : item.item.CategoryName, recipeDetailsData : response.Data.RecipeDetails , Ingredents : response.Data.Ingredients ,refreshList: () => { this.refreshList() }});              
        }
        else {
            console.log('recipe already favourite' + favouriterecipeId) 
        }
    }
    refreshList () {
        this.setState({
            recipesList : [],
            offset : 0,
            searchProgress : true,
            isLoading : true
         
        })
        setTimeout(() => {
            this._getRecipeList()        
        }, 50);
    
    }

    render() {  
        var { recipesList, categoryArray, difficultyArray, selectedArray } = this.state
        var progressView = <View/>
        var searchBarView = <View/>
        var filteredTags;
        var RecipeList = <View/>
        var scrollEnable = this.state.searchListArr.length && this.state.recipesList.length < 0 ? true : false  
        var scrollView ;
        // var isRefreshTags = false       
      
        // this.refreshSelectedTags(isRefreshTags);
        if(this.state.searchBarVisible){
            searchBarView =   
            <SearchBar
            ref = {(search) => this.search = search}
            platform = "ios" //{(Platform.OS == 'ios')? "ios" : "android"}
            containerStyle = {styles.searchContainerStyle}
            inputContainerStyle = {{height:'100%',backgroundColor : 'rgba(233,233,235,1)'}}
            placeholder="Søk etter navn eller ingrediens ..."
            onChangeText={(text) => this.onChangeSearchText(text)} 
            value={this.state.searchRecipe}
            onCancel = {()=> {this.onTapCancelSearchText()}}
            onClear = {()=> {this.onTapClearText()}}
            autoFocus = {true} 
            allowFontScaling = {false}
          />
        } 
        if(this.state.searchProgress ) {
             RecipeList = <View/>
        } else {
             RecipeList = 
            <View style = {styles.notFoundRecipe}>
                <Image source = {Constants.IC_DIFFICULTY} style = {styles.recipeNotFoundImage}/>
                <Text style = {styles.notFoundRecipeText} allowFontScaling = {false}>Ingen oppskrifter funnet</Text>
                <Text style = {styles.notFoundRecipeTextDescription} allowFontScaling = {false}> Vi fant ingen oppskrifter som matchet  ditt søk </Text>
            </View>
            if(this.state.selectedArray.length > 0) {
                RecipeList =        
                <View style = {styles.notFoundRecipe}>
                    <Image source = {Constants.IC_DIFFICULTY} style = {styles.recipeNotFoundImage}/>
                    <Text style = {styles.notFoundRecipeText} allowFontScaling = {false}>Ingen oppskrifter funnet</Text>
                    <Text style = {styles.notFoundRecipeTextDescription} allowFontScaling = {false} numOfLines = {3}> Vi har ikke funnet noen oppskrifter som møter dine filter-kriterier. Forsøk ta vekk noen av filtrene for å finne det du ser etter.</Text>
                </View>        
             }

        }
        
       
        // load data first time , dispaly indicator in center 
        if(recipesList.length == 0  && this.state.searchProgress){
            let animatingLoading = this.state.isLoading ? true : false
            progressView = 
            <View style ={{height:Constants.deviceHeight - 170,width:'100%',justifyContent:'center',alignItems:"center",position  :'absolute' }}>
                <View style = {{backgroundColor : 'rgba(225,225,225,0.7)', borderRadius : 10 , height : 70 ,width : 70 , justifyContent : 'center' ,alignItems : 'center' }}>
                    <ActivityIndicator style ={styles.loading} color = 'red' size ='large' animating = {animatingLoading}/>
                </View>
            </View>
        }

        let flatListData = (this.state.searchListArr.length == 0) ? recipesList : this.state.searchListArr;

        if(this.state.recipesFound){
            
            RecipeList = 
            <FlatList
                style = {styles.flatListStyle}
                renderItem = {this._renderItem}
                keyExtractor = { (item,index) => index.toString() }
                data = { flatListData }
                extraData = {this.state}
                showsVerticalScrollIndicator = {false}
                ItemSeparatorComponent={this.renderSeparator}
                ListFooterComponent={this.renderFooter()}
                onEndReached = {this.handleLoadMore}
                onEndReachedThreshold = {0.4}
            />
        }

        if(this.state.isFilter) {
            if(selectedArray.length > 0) {
               
                filteredTags = <View style={ {marginTop: 10, padding: 10}}>
                        <TagSelect
                            value={[selectedArray]}
                            data={selectedArray}
                            ref={(tag) => { this.tagString = tag; } }
                            itemStyle={styles.itemSelected}
                            itemLabelStyle={styles.labelSelected}
                            itemStyleSelected={styles.itemSelected}
                            itemLabelStyleSelected={styles.labelSelected}
                            onItemPress = {(item) => this.onSelectedItemPress(item)}
                        />
                    </View>
            }
        }
        console.log('recipesList.length : ' , recipesList.length , 'this.state.searchListArr.length  : ' , this.state.searchListArr.length )
        if(recipesList.length > 0 || this.state.searchListArr.length > 0) {
            scrollView = 
                <View style={styles.container}>
                
                <Modal animationType = "fade" transparent = {true} visible = {this.state.modalVisible}>
                    <View style = { styles.modalViewConatiner }>
                        <View style = {styles.modalViewInnerContainer}> 
                            <View style = {styles.filterHeaderView}>
                                <TouchableOpacity style={styles.closeImageTouchable} onPress = {() => { this.setModalVisible(!this.state.modalVisible)}}>
                                    <Image source={Constants.IC_CLOSE} style={styles.closeImage}/>
                                </TouchableOpacity>
                                <Text style={styles.filterHeaderText} allowFontScaling = {false}> Filter Oppskrifter </Text>
                                <TouchableOpacity style={styles.resetTouchable} onPress = {(item) => this.resetTags(item)}>
                                    <Text style={styles.resetText} allowFontScaling = {false}>Reset</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={ styles.deviderViewFilter}/>      

                            <Text style = {styles.filterCategoryText} allowFontScaling = {false}> KATEGORI </Text>

                            <ScrollView style={ { height: Constants.deviceHeight * 0.23 } } contentContainerStyle={ { padding: 10 } } scrollEnabled={true}>
                                <TagSelect
                                    value={selectedArray}
                                    data={categoryArray}
                                    containerStyle={ { width: Constants.deviceWidth - 20 } }
                                    ref={(tag) => { this.categoryTags = tag; } }
                                    itemStyle={styles.item}
                                    itemLabelStyle={styles.label}
                                    itemStyleSelected={styles.itemSelected}
                                    itemLabelStyleSelected={styles.labelSelected}
                                    onItemPress = {(item) => this.onPressCategory(item)} 
                                />
                            </ScrollView>

                            <View style={styles.deviderViewFilter} />

                            <View style = {{justifyContent: 'center', marginTop: 24, padding: 10}}>
                                <Text style = {styles.difficultyFilterText} allowFontScaling = {false}> VANSKELIGHET </Text>
                                <TagSelect
                                    value={selectedArray}
                                    data = {difficultyArray}
                                    containerStyle={ { width: Constants.deviceWidth - 20 } }
                                    ref={(tag) => { this.difficultyTags = tag; } }
                                    itemStyle={styles.item}
                                    itemLabelStyle={styles.label}
                                    itemStyleSelected={styles.itemSelected}
                                    itemLabelStyleSelected={styles.labelSelected}
                                    onItemPress = {(item) => this.onPressDifficulty(item)}
                                />
                            </View>
                        </View>
                    </View>
                </Modal>
                
            
                    {searchBarView}
                    {filteredTags}
                    {RecipeList}
                    {progressView}
                        
            </View>     
        }
        else {
            scrollView = 

            <ScrollView style = {{height :Constants.deviceHeight}} scrollEnable = {true}>
            <View style={styles.container}>
            
                <Modal animationType = "fade" transparent = {true} visible = {this.state.modalVisible}>
                    <View style = { styles.modalViewConatiner }>
                        <View style = {styles.modalViewInnerContainer}> 
                            <View style = {styles.filterHeaderView}>
                                <TouchableOpacity style={styles.closeImageTouchable} onPress = {() => { this.setModalVisible(!this.state.modalVisible)}}>
                                    <Image source={Constants.IC_CLOSE} style={styles.closeImage}/>
                                </TouchableOpacity>
                                <Text style={styles.filterHeaderText} allowFontScaling = {false}> Filter Oppskrifter </Text>
                                <TouchableOpacity style={styles.resetTouchable} onPress = {(item) => this.resetTags(item)}>
                                    <Text style={styles.resetText} allowFontScaling = {false}>Reset</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={ styles.deviderViewFilter}/>      

                            <Text style = {styles.filterCategoryText} allowFontScaling = {false}> KATEGORI </Text>

                            <ScrollView style={ { height: Constants.deviceHeight * 0.23 } } contentContainerStyle={ { padding: 10 } } scrollEnabled={true}>
                                <TagSelect
                                    value={selectedArray}
                                    data={categoryArray}
                                    containerStyle={ { width: Constants.deviceWidth - 20 } }
                                    ref={(tag) => { this.categoryTags = tag; } }
                                    itemStyle={styles.item}
                                    itemLabelStyle={styles.label}
                                    itemStyleSelected={styles.itemSelected}
                                    itemLabelStyleSelected={styles.labelSelected}
                                    onItemPress = {(item) => this.onPressCategory(item)} 
                                />
                            </ScrollView>

                            <View style={styles.deviderViewFilter} />

                            <View style = {{justifyContent: 'center', marginTop: 24, padding: 10}}>
                                <Text style = {styles.difficultyFilterText} allowFontScaling = {false}> VANSKELIGHET </Text>
                                <TagSelect
                                    value={selectedArray}
                                    data = {difficultyArray}
                                    containerStyle={ { width: Constants.deviceWidth - 20 } }
                                    ref={(tag) => { this.difficultyTags = tag; } }
                                    itemStyle={styles.item}
                                    itemLabelStyle={styles.label}
                                    itemStyleSelected={styles.itemSelected}
                                    itemLabelStyleSelected={styles.labelSelected}
                                    onItemPress = {(item) => this.onPressDifficulty(item)}
                                />
                            </View>
                        </View>
                    </View>
                </Modal>
                
              
                    {searchBarView}
                    {filteredTags}
                    {RecipeList}
                    {progressView}
                          
            </View>     
            </ScrollView>       
        }

        return (
           <View style = {{height : '100%',width : '100%'}}>
               {scrollView}
           </View> 
          
        );
    }
}

AllRecipes.PropTypes = {
    onReceipeTap: PropTypes.func,
    navigation: PropTypes.any
}

AllRecipes.defaultProps = {
    onReceipeTap: null,
    navigation: null
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
    },
    flatListStyle: {
        height :  Constants.deviceHeight ,
        width : Constants.deviceWidth,        
    },
    itemContainer: {
        height : 336,
        width : Constants.deviceWidth,
        flexDirection : 'column',
    },
    recipeItem: {
        height : 336,
        width : Constants.deviceWidth,
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center'
    },
    recipeImage: {
        height : 224,
        width : Constants.deviceWidth-24,
        borderRadius : 4,
    },
    recipeContent: {
        height : 90, 
        flex: 1,
        marginRight : 3,
        marginLeft : 4,
        justifyContent : 'center'
    },
    recipeContentText: {
        height : 20,
        fontSize : 15,
        marginRight : 10    
    },
    loading: {
        left : 0,
        right : 0,
        top : 0,
        bottom : 0,
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : 'rgba(0,0,0,0)',
        // position : 'absolute'
    },
    renderFooter: {
        height : '10%',
        width : '100%',
        marginTop : 10,
        justifyContent : 'center',
        alignItems : 'center',    
    },
    recipeName: {
        fontSize : 18, 
        fontFamily : Constants.RobotoBlack,
        lineHeight : 24, 
        fontWeight : 'bold', 
        width : Constants.deviceWidth-24, 
        marginTop : '2%',
        color : Constants.RECIPE_NAME_COLOR,
        fontFamily : Constants.RobotoMedium,
    },
    authorName: {
        fontSize : 14, 
        lineHeight : 20,
        fontFamily : Constants.RobotoMedium,
        fontWeight: '500', 
        width : Constants.deviceWidth-24, 
        marginTop : 3,
        height : 22, 
        color : Constants.RECIPE_AUTHOR_NAME_COLOR,
    },
    durationView: {
        width : Constants.deviceWidth-24,
        height : '5%',
        flexDirection : 'row',
        alignItems : 'center',
        marginTop : 4
    },
    durationContent: {
        height: 20 , 
        color: 'rgba(113,119,127,1)', 
        fontSize: 14, 
        lineHeight: 20,
        textAlign:'left',
        fontFamily : Constants.RobotoRegular
    },
    notFoundRecipe: {
        // marginTop : Constants.deviceHeight * 0.25,
        marginTop : 50,
        // height : '100%' ,
         width : '100%', 
        justifyContent : 'center',
        alignItems : 'center',
        // position : 'absolute',
    },
    notFoundRecipeText: {
        height: 32,
        width: Constants.deviceWidth - 48,
        color: 'rgba(28,40,51,1)',
        fontSize: 24,
        fontWeight: 'bold',
        lineHeight: 32,
        textAlign: 'center',
        fontFamily : Constants.RobotoMedium,
    },
    recipeNotFoundImage: {
        height : 50,
        width : 50,
    },
    favouriteImage: {
        height : 30,
        width : 33,
    },
    favouriteTouchable: {
        height : Constants.deviceWidth * 0.11 , 
        width : Constants.deviceWidth * 0.11, 
        marginLeft: (Constants.deviceWidth - 5) * 0.84, 
        marginTop: 25, 
        backgroundColor : 'white',
        borderRadius :(Constants.deviceWidth * 0.11) / 2 , 
        position : 'absolute' ,
        alignItems : 'center',
        justifyContent : 'center' 
    },
    notFoundRecipeTextDescription : {        
        width: Constants.deviceWidth - 48,
        color: 'rgba(62,72,82,1)',        
        fontSize: 16,
        lineHeight: 24,
        textAlign: 'center', 
        marginTop : 12 ,
        fontFamily : Constants.RobotoRegular,     
    }, 
    item: {
        borderWidth: 1,
        borderColor: '#333',    
        backgroundColor: '#FFF',
        marginBottom: 10,
    },
    label: {
        color: '#333',
        fontSize: 14,
        fontFamily: Constants.RobotoRegular,
    },
    itemSelected: {
        borderWidth: 1,
        borderColor: '#f35353',
        backgroundColor: '#f35353',
    },
    labelSelected: {
        color: '#FFF',
    },
    buttonContainer: {
        padding: 15,
    },
    buttonInner: {
        marginBottom: 15,
    },
    modalViewConatiner: {        
        width : Constants.deviceWidth,
        ...Platform.select({
            android: {
                marginTop: 0,
                height : Constants.deviceHeight - 48 - StatusBar.currentHeight,
            },
            ios: {
                marginTop: (Constants.deviceHeight >= 812) ? 40 : 20,
                height :(Constants.deviceHeight >= 812) ? Constants.deviceHeight - 82 - 40 : Constants.deviceHeight - 48 - 20 ,
            }
        }),
        backgroundColor: 'rgba(0,0,0,0.3)'
    },
    filterHeaderText: {
        width: Constants.deviceWidth - (Constants.deviceHeight * 0.061) * 2 - 15, 
        lineHeight: Constants.deviceHeight * 0.061, 
        textAlign: 'center', 
        fontSize: 18, 
        fontFamily: Constants.RobotoMedium,
        fontWeight : '500',
        color  :'rgba(28,40,51,1)' 
    },
    resetText: {
        height: Constants.deviceHeight * 0.061, 
        width: Constants.deviceHeight * 0.061, 
        lineHeight: Constants.deviceHeight * 0.061, 
        textAlign: 'center', 
        color: 'rgba(113,119,127,1)', 
        fontFamily: Constants.RobotoMedium ,
        fontSize : 14 ,
        marginRight :16 ,
        fontWeight : '500',        
    },
    filterCategoryText: {
        marginLeft: 20, 
        marginTop: 24, 
        marginBottom: 16, 
        fontSize: 14, 
        fontFamily: Constants.RobotoMedium , 
        fontWeight : '500',
        color  :'rgba(62,72,82,1)' 
    },
    difficultyFilterText: {
        marginLeft: 20,
        marginBottom: 16, 
        fontSize: 14, 
        fontFamily: Constants.RobotoMedium ,
        fontWeight : '500',
        color  :'rgba(62,72,82,1)'
    },
    modalViewInnerContainer: {
        marginTop: 0,
        height: 400, 
        width: Constants.deviceWidth,
        backgroundColor: 'white'
    },
    filterHeaderView: {
        flexDirection: 'row',
        marginTop: 0,
        height: Constants.deviceHeight * 0.061, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    closeImageTouchable: {
        height: Constants.deviceHeight * 0.060, 
        width: Constants.deviceHeight * 0.060,
        justifyContent :"center",
        alignItems : 'center',        
    },
    closeImage: {
        height: '80%', 
        width: '80%', 
        resizeMode: 'center' 
    },
    resetTouchable: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'
    },
    deviderViewFilter: {
        backgroundColor: 'rgba(235,235,235,1)', 
        height: 1, 
        width: Constants.deviceWidth
    },
    searchContainerStyle: {
        height : 50,
        width : Constants.deviceWidth-16,
        backgroundColor : 'white',
        alignSelf:'center',
        borderTopWidth : 0,
        borderBottomWidth:0,
        marginLeft:8,
        marginRight:8,
        ...Platform.select({
            android: {
                marginTop: 0,
            },
            ios: {
                marginTop: (Constants.deviceHeight >= 812) ? 40 : 20,
            }
        }),
    }
})