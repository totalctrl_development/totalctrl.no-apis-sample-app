import React from "react";
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native'

import  { SearchBar }  from 'react-native-elements'; 

import *  as Constants from '../common/Constants' 

export default class RecipeList extends React.Component {
   
    static navigationOptions = ({ navigation }) => ({
        title: "Huskeliste",
        headerTintColor: 'black',
        headerTitleStyle: {
            alignSelf:'center',
            flex: 1,
            justifyContent: 'center',
            textAlign: 'center',
            fontSize:Constants.HEADER_FONT_SIZE,
            fontFamily: Constants.RobotoMedium,
            fontWeight:'500',
            allowFontScaling : false,
            color  : 'rgba(28,40,51,1)'
        },
        headerRight : <View style ={styles.headeright}>
        <Text style ={{fontWeight : '500'}}>{"Rydd liste"}</Text>
        </View>
    });

    constructor (){
        super()
        this.state = {
            searchText  : '',
            wishList : [
                { id: "1", name: "Bananer" , quantity : 0,},
                { id : "2", name  : "Brød" , quantity : 0,},
                { id : "3", name  : "Egg" , quantity : 0,},
                { id : "4", name  : "Gulost" , quantity : 0,},
                { id : "5", name  : "Juice" , quantity : 0,},
                { id : "6", name  : "Rømme",  quantity : 0,},
                { id : "7", name  : "Saft", quantity : 0,},
                { id : "8", name  : "Bananer", quantity : 0,},
                { id : "9", name  : "Bananer", quantity : 0,},
            ],
            wishListSearchArr : []
        }
    }

    componentDidMount() {
        this.setState({
            wishListSearchArr : this.state.wishList
        })
    }

    renderItem = (item,index) => {
        var wishList = item.item
        return (
            <View style ={styles.flateListCell}>
                
                <View style = {styles.quantityView}>
                    <View style = {styles.quantityTextView}>
                    <Text  >{wishList.quantity}</Text>
                    </View>
         
                    <View style ={styles.arrowImageView}>
                        <View style = {styles.arrowUpDownImageView}>
                        <TouchableOpacity  onPress = {() => this.upArrowTap(item,index)}>
                            <Image style = {styles.arrowImage} source = {require('../res/uparrow.png')}/>
                        </TouchableOpacity>
                        </View>
                        <View style = {styles.arrowUpDownImageView}>
                        <TouchableOpacity  onPress = {() => this.downArrowTap(item,index)}>
                            <Image style = {styles.arrowImage} source = {require('../res/downarrow.png')}/>
                        </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style = {styles.recipeNameView}>
                    <Text>{wishList.name}</Text>
                </View>

                <View style = {styles.removeIconView}>
                    <Image style = {styles.removeIconImage} source = {require('../res/delete.png')}/>
                </View>
            </View>
        )
    }

    renderSeprator () {
        return (
            <View style = {{height :1,width :'100%',backgroundColor:'black'}}></View>
        )
    }

    searchFilterFunction = (text) => {    
        this.setState({
            searchText : text
        })
        const newData = this.state.wishList.filter(item => {      
            const itemData = `${item.name.toUpperCase()}`;
            const textData = text.toUpperCase();
            
            return itemData.indexOf(textData) > -1;    
        });
        this.setState({ wishListSearchArr: newData });  
    }

    upArrowTap(item, index){        
        let  data = this.state.wishList[item.index]

        data.quantity = parseInt(data.quantity)  + 1
        this.state.wishList[item.index] = data
        this.setState({wishList : this.state.wishList})
       
    }

    downArrowTap(item, index) {
        let  data = this.state.wishList[item.index]
        if(data.quantity == 0) {
            console.log()
        }
        else {
            data.quantity = parseInt(data.quantity)  - 1
            this.state.wishList[item.index] = data
            this.setState({wishList : this.state.wishList})
        }
       
    }
    
    render() {
        var{wishListSearchArr} = this.state
        return (
            <View>
                
            </View>
            // <View styles = {styles.container}>
                
            //     <SearchBar
            //         platform = "ios"
            //         containerStyle = {{height : '10%',width : '100%'}}
            //         inputContainerStyle = {{backgroundColor:"white",height:'20%'}}
            //         placeholder="Legg til varer"
            //         onChangeText={(text) => this.searchFilterFunction(text)}
            //         value={this.state.searchText}
            //      />
            
            //     <FlatList 
            //         style  = {styles.flatListStyle} 
            //         showsVerticalScrollIndicator = {false}
            //         data = {wishListSearchArr}
            //         extraData = {this.state}
            //         keyExtractor = { (item, index) => `${index}` }
            //         renderItem  = {this.renderItem.bind(this)}
            //         ItemSeparatorComponent  = {this.renderSeprator}
            //     />
            // </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      height :'100%',
      width : '100%'
    },
    imageStyle: {
        height : 600,
        width : 300,
        justifyContent : 'center',
        alignItems : 'center',
        alignSelf : 'center'
    },
    flatListStyle: {
        height : '90%',
        width :  '100%',
    },
    searchViewStyle :{
        height : '10%',
        width :  '100%',
    },
    flateListCell: {
        height : 70,
        width: '100%',
        flexDirection: 'row',
        // backgroundColor : 'yellow'
    },
    quantityView: {
        height : '100%',
        width : '15%',
        justifyContent : 'center',
        alignItems : 'center',
        flexDirection: 'row'
        // backgroundColor  :'green'
    },
    recipeNameView: {
        height : '100%',
        width : '60%',
        justifyContent : 'center',
        alignItems : 'flex-start',
        marginLeft  :10
    },
    headeright: {
        width : '100%',
        height : '100%',
        justifyContent : 'center',
        marginRight : 10
    },
    quantityTextView : {
        height  : '40%',
        width : '60%',
        borderWidth : 2,
        borderColor : 'black',
        marginLeft : 5,
        justifyContent : 'center',
        alignItems : 'center'
    },
    arrowImageView : {
        height  :'40%',
        width: '40%',
        borderWidth : 2
    },
    arrowUpDownImageView : {
        height : '50%',
        width  : '100%',
        justifyContent : 'center',
        alignItems : "center"
    },
    arrowImage : {
        height : 15,
        width: 15
    },
    removeIconView: {
        height : '100%',
        width : '20%',
        justifyContent : 'center',
        alignItems : 'center'
    },
    removeIconImage : {
        height : 30 ,
        width : 30
    }

})
