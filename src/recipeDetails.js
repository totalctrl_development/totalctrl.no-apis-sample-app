import React from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Animated,
    ScrollView,
    FlatList,
    Image,
    NetInfo
} from 'react-native'
import * as  Constants from '../common/Constants'
import * as APIHelper from '../helper/apiHelper'
import EventEmitter from "react-native-eventemitter";
// import Image from 'react-native-remote-svg';

function rightBarButton(state) {
    let imageFavourite = state.params.isFavourite ? Constants.IC_FAVORITE_SELECTED : Constants.IC_FAVORITE_DESELECTED
    return (
        <View style = {{width : '100%', marginRight : 15, justifyContent: 'center' }}>
            <TouchableOpacity activeOpacity = {0.7} onPress = {() => state.params.handleRightAction()}>
                <Image source = {imageFavourite} style = {{height: 30 ,width: 30}}/>
            </TouchableOpacity>
        </View>
    )
}

function leftBarButton(state) {
   return (
    <View style = {{width :'100%',marginLeft : 15}} >
        <TouchableOpacity onPress = {() => state.params.handleDoneAction()}>
            <Image source = {Constants.HEADER_BACK_ICON} style = {{height: 25 ,width: 25}}/>
        </TouchableOpacity>
    </View>
   )
}

export default class recipeDetails extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitle:   <Text style = {{fontSize : 18, fontWeight : '500', fontFamily: Constants.RobotoMedium, color: 'black' }} numberOfLines = {1} allowFontScaling={false}>{navigation.state.params.title}</Text> ,
        headerTintColor: 'black',       
        headerLeft  : leftBarButton(navigation.state),
        headerRight : rightBarButton(navigation.state)
    });

    constructor() {
        super()
        this.state = {
            enableScrollViewScroll: true,
            active: 0,
            xTabOne: 0,
            xTabTwo: 0,
            translateX: new Animated.Value(0),
            translateXTabOne: new Animated.Value(0),
            translateXTabTwo: new Animated.Value(Constants.deviceWidth),
            translateY: -1000,
            recipeDetail : [],
            Ingredents  :[],
            ownerName : '',
            title : '',
            categoryName : ''
        }
    }

    componentWillMount() {        
        this.setState({
            recipeDetail : this.props.navigation.state.params.recipeDetailsData,
            Ingredents : this.props.navigation.state.params.Ingredents,
            ownerName : this.props.navigation.state.params.ownerName,
            title : this.props.navigation.state.params.title,
            categoryName :  this.props.navigation.state.params.categoryName
        }) 
        this.props.navigation.setParams({
            handleDoneAction: this.onTapLeftButton.bind(this),  
            handleRightAction : this.onTapRightButton.bind(this),  
        });
    }

    componentDidMount() {
    }
    
    onTapLeftButton = () => {
        this.props.navigation.setParams({ 
            SearchBar: true ,
            header:  undefined,          
            SearchBar: undefined ,
            searchText : ''
        });
        this.props.navigation.state.params.refreshList()        
        this.props.navigation.goBack(null)    
        
    }

    onTapRightButton = () => {
       
        // this.props.navigation.setParams({
        //     isFavourite : true
        // })
        if(this.props.navigation.state.params.isFavourite){
            this.removeFavourite()
        }
        else{
           this.addFavourite()
        }
       
    }
    
    removeFavourite () {
        
        let url =  APIHelper.baseURL + APIHelper.removeFavourite 
        NetInfo.isConnected.fetch().then(isConnected =>{
            if(isConnected) {
                fetch(url,{
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-type': 'application/json',
                        'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
                    },
                    body: JSON.stringify({
                        UserId : 1,
                        RecipeId : this.state.recipeDetail.Id
                    })
                })
                .then((response) => response.json())
                // .then(json => this._handleResponse(json.response))
                .then((responseJson) => {
                    this.removeFavouriteResponse(responseJson);
                })
                .catch((error) => {
                    console.error(error);
                });    
                
            }
            else{
                Alert.alert(Constants.NETWORK_ALERT_TITLE, Constants.NETWORK_ALERT_MESSAGE)
                return
            }
        })
    }

    addFavourite () {
        let url =  APIHelper.baseURL + APIHelper.addFavourite 
        NetInfo.isConnected.fetch().then(isConnected =>{
            if(isConnected) {
                fetch(url,{
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-type': 'application/json',
                        'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
                    },
                    body: JSON.stringify({
                        UserId : 1,
                        RecipeId : this.state.recipeDetail.Id
                    })
                    })
                    .then((response) => response.json())
                    // .then(json => this._handleResponse(json.response))
                    .then((responseJson) => {
                        this.getAddFavouriteResponse(responseJson);
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            }
            else {
                Alert.alert(Constants.NETWORK_ALERT_TITLE, Constants.NETWORK_ALERT_MESSAGE)
                return
            }
        })
       
    }

    getAddFavouriteResponse = (responseJson) => {
        // alert(JSON.stringify(responseJson.Status))
        if(responseJson.Status){
            this.props.navigation.setParams({
                isFavourite : true
            })
        }
        else{
            console.log('error ' + JSON.stringify(responseJson))
        }
    }

    removeFavouriteResponse = (responseJson) => {
        // alert(JSON.stringify(responseJson.Status))
        if(responseJson.Status){
            this.props.navigation.setParams({
                isFavourite : false
            })
        }
        else{
            console.log('error ' + JSON.stringify(responseJson))
        }
    }

    handleSlide = type => {
        let { xTabOne, xTabTwo, translateX, active, translateXTabOne, translateXTabTwo } = this.state;
        Animated.spring(translateX, {
            toValue: type,
            duration: 100
        }).start();
        if(active === 0){
            Animated.parallel([
                Animated.spring(translateXTabOne, {
                    toValue: 0,
                    duration: 100
                }).start(),
                Animated.spring(translateXTabTwo, {
                    toValue: Constants.deviceWidth,
                    duration: 100
                }).start()
            ])
        }else{
            Animated.parallel([
                Animated.spring(translateXTabOne, {
                    toValue: -Constants.deviceWidth,
                    duration: 100
                }).start(),
                Animated.spring(translateXTabTwo, {
                    toValue: 0,
                    duration: 100
                }).start()
            ])
        }
    }

    renderItem ({item}) {
        return(
            <View style = {{ width: '100%', alignItems: 'center'}}>
               <Text style = {styles.instructions} allowFontScaling={false}>{item}</Text>
            </View>
        )
    }
    renderItem1 ({item}) {
        let filteredArray = window.measurementsArray.filter((object) => {
            return object.Id == item.MeasurementId;
          });

        let measurementText = (filteredArray.length > 0) ? filteredArray[0].ShortName : '';
        return(
            <View style = {{height :50,width: '100%', alignItems: 'center',flexDirection : 'row'}}>
               <Text style = {[styles.ingredientName,{marginLeft : 10 ,width  :'80%'}]} allowFontScaling={false}>{item.Title}</Text>
               <Text style = {styles.ingredientName} allowFontScaling={false}>{item.PortionQuantity + measurementText}</Text>
            </View>
        )
    }

    space(){
        return(
            <View style = {{height: 5, backgroundColor : 'rgba(233,233,233,0.5)', width: '100%'}}>
            </View>
        )
    }

    space1(){
        return(
            <View style = {{height: 1, backgroundColor : 'rgb(232,232,232)', width: '100%'}}>
            </View>
        )
    }

    render() {
        let { xTabOne, xTabTwo, translateX, active, translateXTabTwo, translateXTabOne, translateY,recipeDetail} = this.state;
        var difficulty=  recipeDetail.Difficulty == 0 ? "Lett" : recipeDetail.Difficulty == 1 ? "Middels" : "Vanskelig"
        let hours =parseInt(recipeDetail.Duration) / 60
        let minutes =parseInt(recipeDetail.Duration) % 60
        var duration = ""
        if (parseInt(hours) > 0) {
            if (minutes > 0) {
                duration = parseInt(hours) + " h" + parseInt(minutes) + ' min'
                
            }
            else {
                duration = parseInt(hours) + " h"                              
            }
        }
        else {
            duration = parseInt(minutes) + ' min'
            
        }
       
        return (
            <View styles = {styles.container}>
                <ScrollView showsVerticalScrollIndicator = {false} bounces = {false} >
                <View style = {{height : Constants.deviceHeight * 0.395, width : Constants.deviceWidth}}>
                    <Image source = {{uri : recipeDetail.Image }} style = {{height : '100%',width : '100%',resizeMode : 'cover'}}/>
                </View>
                <View style = {styles.categoryButton}>
                    <Text style = {styles.CategoryText} allowFontScaling = {false}>{this.state.categoryName}</Text>
                </View>
                <View style = {styles.recipeTextContainer}>
                    <View style = {styles.detailTextConatiner}>
                       <Text style = {styles.recipeNameText} allowFontScaling = {false}>{this.state.title}</Text>
                       <Text style = {styles.authorText} allowFontScaling = {false}>{this.state.ownerName}</Text>
                        <View style = {styles.recipeInfoContainer}>
                            <View style ={styles.recipeImageContainer} >
                                <View style= {styles.recipeInfo}>
                                    <Image source = {Constants.IC_DURATION} style = {styles.imageStyle}/>
                                    <Text style = {styles.detailText} allowFontScaling = {false} >{duration}</Text>
                                </View>
                                <View style= {styles.recipeInfo}>
                                    <Image source = {Constants.IC_PORTIONS} style = {styles.imageStyle}/>
                                    <Text style = {styles.detailText} allowFontScaling = {false} >{recipeDetail.Portions + " prosj."} </Text>
                                </View>
                                <View style= {styles.recipeInfo}>
                                    <Image source = {Constants.IC_DIFFICULTY} style = {styles.imageStyle}/>
                                    <Text style = {styles.detailText} allowFontScaling = {false} >{difficulty}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
               </View>

                <View style = {styles.TabMainView}>
                    <Animated.View style = {{position: 'absolute', width: '50%', height: 4, left: 0, backgroundColor: 'pink', bottom: 0, transform: [{ translateX }]}} />
                    <TouchableOpacity 
                        style = {{justifyContent: 'center', alignItems: 'center', flex: 1}}
                        onLayout = {event => this.setState({ xTabOne: event.nativeEvent.layout.x })}
                        onPress = {() => this.setState({active: 0}, () => this.handleSlide(xTabOne))}
                        activeOpacity = {0.9}
                    >
                        <Text style = {[styles.tabViewTextStyle, {color: active === 0 ? 'rgba(28,40,51,1)' : 'rgba(113,119,127,1)'}]} allowFontScaling = {false}>
                            INGREDIENSER
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        style = {{justifyContent: 'center', alignItems: 'center', flex: 1}}
                        onLayout = {event => this.setState({ xTabTwo: event.nativeEvent.layout.x })}
                        onPress = {() => this.setState({active: 1}, () => this.handleSlide(xTabTwo))}
                        activeOpacity = {0.9}
                    >   
                        <Text  style = {[styles.tabViewTextStyle, {color: active === 1 ? 'rgba(28,40,51,1)' : 'rgba(113,119,127,1)'}]} allowFontScaling = {false}>
                            FRAMGANGSMÅTE
                        </Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <Animated.View 
                        style = {{
                            transform: 
                            [{
                                translateX: translateXTabOne
                            }],
                            justifyContent: 'center', 
                            alignItems: 'center',
                        }}
                        onLayout = {event => this.setState({translateY: event.nativeEvent.layout.height})}
                    >
                    <FlatList 
                        style = {{backgroundColor : 'rgba(233,233,233,0.5)',width :'100%',height : '50%'}}
                        data = {this.state.Ingredents}
                        renderItem = {this.renderItem1.bind(this)}
                        ItemSeparatorComponent = {this.space1}
                        extraData = {this.state}
                        scrollEnabled = {this.state.enableScrollViewScroll}
                        keyExtractor = {(index)=> `${index}` }
                    /> 
                    </Animated.View>
                    
                    <Animated.View 
                        style = {{
                            transform: [
                                {
                                    translateX: translateXTabTwo, 
                                },
                                {
                                    translateY: -translateY
                                }
                            ], 
                            alignItems: 'center'
                        }}
                    >
                      <FlatList 
                            style = {{backgroundColor : 'rgba(233,233,233,0.5)',width :'100%',height : '100%'}}
                            data = {this.state.recipeDetail.Description}
                            renderItem = {this.renderItem.bind(this)}
                            ItemSeparatorComponent = {this.space}
                            extraData = {this.state}
                            scrollEnabled = {this.state.enableScrollViewScroll}
                            keyExtractor = {(index)=> `${index}` }
                    /> 
                    </Animated.View>
                </View>

            </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
    },
    headeright: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        marginRight: 10
    },
    categoryButton: {
        backgroundColor: 'rgba(243,83,83,1)',
        height: Constants.deviceHeight * 0.049,
        width: Constants.deviceWidth * 0.197,
        marginTop: -(Constants.deviceHeight * 0.049)/2,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: 8,
        borderRadius: 6,
        borderStyle: 'solid'
    },
    recipeNameText: {
        fontSize: 32,
        fontFamily: Constants.RobotoMedium,
        fontWeight: 'bold',
        lineHeight: 40,
        color: 'rgba(28,40,51,1)'
    },
    authorText: {
        fontSize: 16,
        lineHeight : 24,
        fontFamily: Constants.RobotoRegular,
        fontWeight  : '500',
        color  : 'rgba(243,83,83,1)',
        marginTop : 10
    },
    recipeImageContainer: {
        flexDirection: 'row',
        height: '100%', 
        justifyContent: 'space-between', 
        alignItems: 'center',        
        width : '100%'
    },
    durationView: {
        flexDirection : 'row'
    },
    detailText: {
        fontSize: 14,
        fontFamily: Constants.RobotoRegular,
        color: 'rgba(113,119,127,1)',
        lineHeight: 20,      
    },
    imageStyle: {
        height : 25 ,
        width : 25,
    },
    TabMainView: {
        flexDirection: 'row',
        position: 'relative',
        height: 50,
        borderTopWidth: 0.5,
        borderColor  :'rgb(232,232,232)',
        marginTop : 40
    },
    tabViewTextStyle: {       
        height: 24,
        width: 164,
        fontSize: 15,
        fontFamily: Constants.RobotoMedium,
        fontWeight: '500',
        letterSpacing: 1,
        lineHeight: 24,
        textAlign: 'center',
    },
    CategoryText: {
        height: 24,
        width: 51,
        color: 'rgba(255,255,255,1)',
        fontFamily: Constants.RobotoRegular,
        fontSize: 16,
        lineHeight: 24,
        textAlign: 'center', 
    } ,
    detailTextConatiner: {
        // height :'100%',
        width : Constants.deviceWidth  - 48,        
        justifyContent : 'space-between'
    },
    recipeInfoContainer : {
        height : Constants.deviceHeight * 0.030,
        width : Constants.deviceWidth - 54,        
        marginTop : 10,
        justifyContent : 'center',
        alignItems : 'center'
    },
    recipeTextContainer: {
        // height : Constants.deviceHeight * 0.365,
        width : Constants.deviceWidth,      
        justifyContent : 'center',
        alignItems : 'center'
     },
     recipeInfo: {
        height : '100%',
        width : '33.33%',
        flexDirection : 'row',
        alignItems : 'center'
     },
     instructions: {
        width: 312,
        color: 'rgba(62,72,82,1)',
        // font-family: Roboto;
        fontFamily: Constants.RobotoRegular,
        fontSize: 16,
        lineHeight: 24
    },
    ingredientName :  {
        height: 24,
        color: 'rgba(62,72,82,1)',
        // font-family: Roboto;
        fontFamily: Constants.RobotoRegular,
        fontSize: 16,
        lineHeight: 24
    }
    
})
