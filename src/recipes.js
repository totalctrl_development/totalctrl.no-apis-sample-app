import React from "react";
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Platform,
    Keyboard,
    Image
} from 'react-native'

import * as Constants from '../common/Constants'

import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
// import Image from 'react-native-remote-svg';
import EventEmitter from "react-native-eventemitter";

import  { 
  SearchBar,
  Badge 
}  from 'react-native-elements'; 

import AllRecipes from './allRecipes'
import FavouriteRecipe from './favouriteRecipe'


const AllRecipesRoute = () => (
  <AllRecipes/>
  // <RecipeContainer isForAccepted = { true } onRideTouch = {(item) => {
  //   this.props.navigation.navigate('recipes');
  // }}/>
);
const SecondRoute = () => (
    <FavouriteRecipe/>
);

function rightBarButton(state) {
  
  var rightHeaderImage = state.params.hideSearchButton ? <View/> : <TouchableOpacity style = {{ justifyContent : 'center',alignItems : 'center',height : '100%',width : '100%' }} onPress = {() => state.params.handleRightAction()}>
  <Image  source = {Constants.HEADER_SEARCH_ICON} style = {{height : 30,width : 30,marginRight : 10}}/>
  </TouchableOpacity>
  return(
    rightHeaderImage
  );
}

function leftBarButton(state) {
  var badgeIcon = state.params.isFilterBadge ? <Badge badgeStyle = {{backgroundColor: '#f35353', marginTop: 10, height: 12, width: 12, borderRadius: 6}} /> : null 
  var rightHeaderImage = state.params.hideFilterButton ? <View /> : <TouchableOpacity style = {{height : '100%',width : '100%',flexDirection : 'row' }} onPress = {() => state.params.handleLeftAction()}>
  <Image  source = {Constants.HEADER_FILTER_ICON} style = {{height: 30,width: 30,marginLeft: 5,alignSelf: 'center'}}/>
  {badgeIcon}
  </TouchableOpacity>
  
  return(
    rightHeaderImage
  );
}


var heightOfTabTop = 48
var searchBarView = false

export default class MyKitchen extends React.PureComponent {
   
    static navigationOptions = ({ navigation }) => ({
      header: navigation.state.params ? navigation.state.params.header : undefined,

      //   headerTitle: searchBarView  ? 
      //     <SearchBar 
      //       ref={search => this.search = search}
      //       platform = "android"//{(Platform.OS == 'ios')? "ios" : "android"}
      //       containerStyle = {{height : 50,width : Constants.deviceWidth-16,backgroundColor : 'white',position : 'absolute',alignSelf:'center',borderTopWidth : 0,borderBottomWidth:0,marginLeft:8,marginRight:8}}
      //       inputContainerStyle = {{height:'100%',backgroundColor : 'rgba(233,233,235,1)'}}
      //       placeholder="Søk etter navn eller ingrediens ..."
      //       onChangeText={navigation.getParam('searchFunc')} 
      //       value={navigation.state.params.searchText}
      //       onCancel = {navigation.getParam('onTapSearchCancel')}
      //       onSubmitEditing = {navigation.getParam('onTapSearchCancel')}
      //       autoFocus = {true}
      //     />
      //  :  <Text style = {{fontSize : 18, fontWeight : 'bold',width :'100%',textAlign : 'center',fontFamily : Constants.RobotoBlack}} numberOfLines = {1}>{"Oppskrifter"}</Text>,
      
      headerTitle: searchBarView  ? null : 'Oppskrifter',

       headerStyle :{
        borderBottomWidth : 0,
        elevation : 0
       },
        headerTintColor: 'black',
        headerTitleStyle: {
            alignSelf:'center',
            flex: 1,
            justifyContent: 'center',
            textAlign: 'center',
            fontSize:Constants.HEADER_FONT_SIZE,
            fontFamily: Constants.RobotoMedium,
            fontWeight:'500',
            allowFontScaling : false,
            color  : 'rgba(28,40,51,1)'
        },
        headerRight : rightBarButton(navigation.state),
        headerLeft : leftBarButton(navigation.state)
      
    });
    

    state = {
      index: 0,

      routes: [
          { key: 'First', title: 'ALLE', navigation: this.props.navigation },
          { key: 'Second', title: 'FAVORRITER', navigation: this.props.navigation }
        ],
    }
    componentWillMount () {
      this.props.navigation.setParams({ 
        SearchBar: true ,
        header:  undefined,          
        SearchBar: undefined ,
        searchText : ''          
      });
      searchBarView = false
      heightOfTabTop = 48

    }
   
    // componentDidMount () {
    //   EventEmitter.on("BrowseTap", (value)=>{
    //    this.setState({
    //      index  : 0
    //    })
        
    //   });
    //   this.props.navigation.setParams({
    //     handleRightAction: this.onTapRightButton.bind(this),  
    //     handleLeftAction: this.onTapLeftButton.bind(this),
    //   });
    //   this.props.navigation.setParams({searchFunc: this.searchFunction})
    //   this.props.navigation.setParams({onTapSearchCancel: this.onTapCancel})
    // }

    componentDidMount () {
      this.props.navigation.setParams({
        handleRightAction: this.onTapRightButton.bind(this),  
        handleLeftAction: this.onTapLeftButton.bind(this),
        searchFunc: this.searchFunction,
        onTapSearchCancel: this.onTapCancel        
      });
      
      EventEmitter.on("BrowseTap", (value)=>{
        this.setState({
          index  : 0
        })
      })    
      EventEmitter.on("onTapCancel", (value)=>{
        this.props.navigation.setParams({ 
          SearchBar: true ,
          header:  undefined,          
          SearchBar: undefined ,
          searchText : ''          
        });
        searchBarView = false
        heightOfTabTop = 48
      }) 
    }


    componentWillUnmount() {
      this.props.navigation.setParams({searchFunc: null})
    }
  
    onTapRightButton =() => {
      this.props.navigation.setParams({ 
        SearchBar: true ,
        header:  undefined,          
        SearchBar: undefined ,
        searchText : ''
      });
      setTimeout(() => {
        this.props.navigation.setParams({header: null})
        this.props.navigation.setParams({ 
          SearchBar: true
        });
        heightOfTabTop = 0
        searchBarView = true
        EventEmitter.emit("SearchBar")
      }, 300);
      
    }


    // componentWillUnmount() {
    //   this.props.navigation.setParams({searchFunc: null})
    // }
  
    // onTapRightButton =() => {
    //     this.props.navigation.setParams({ 
    //         SearchBar: true 
    //     });
    //     this.props.navigation.setParams({ 
    //       header: undefined
    //   });
    //     heightOfTabTop = 0
    //     searchBarView = true
    // }

    onTapLeftButton =() => {
      EventEmitter.emit("ModalVisible")
    }


    searchFunction = (text) => {
      this.props.navigation.setParams({searchText:text})
      EventEmitter.emit("searchText",text);
    }

    onTapCancel = () => {
      this.props.navigation.setParams({ 
        SearchBar: undefined ,
        searchText : ''
      });
      searchBarView = false
      heightOfTabTop = 48
      EventEmitter.emit("onTapCancel");
    }

    changeIndex = (index) => {
      let changeIndex = index 
      this.setState({
        index : index
      })
      if(changeIndex === 0){
        this.props.navigation.setParams({
          hideSearchButton: false,
          hideFilterButton: false
        })
      }
      else if (changeIndex === 1 ){
        EventEmitter.emit("changeTabIndex");
        EventEmitter.emit("changeTabIndex1");
        this.props.navigation.setParams({
          hideSearchButton: true,
          hideFilterButton: true
        })
      }
     
    }

    onReceipeTap = (item) => {
      this.props.navigation.navigate('RecipeDetails', {recipeDetail : item.item , title : item.item.Name , isFavourite  : item.item.IsFavorites});
    }

    render(){
        return (
           
          <TabView 
          navigationState={this.state}        
          renderScene={SceneMap({
            First: () => <AllRecipes navigation={this.props.navigation}/>,
            Second: () => <FavouriteRecipe navigation={this.props.navigation}/>,
          })}
          onIndexChange={index => {this.changeIndex(index)}}        
          initialLayout={{ width: Dimensions.get('window').width }}
          swipeEnabled = {false}
          renderTabBar = { props =>
          <TabBar
            {...props}
            indicatorStyle = {{ backgroundColor:  Constants.ACTIVE_TINT_COLOR, height: 5 }}
            labelStyle = {styles.topTab}
            style = {{ backgroundColor: 'white',height :heightOfTabTop}}
            getLabelText={({ route }) => route.title}  
            allowFontScaling={false}
          />
        }
      />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1 
    },
    topTab: { 
     height : 24,
     width : '100%',
     color: 'rgba(28,40,51,1)',
     fontFamily : Constants.RobotoMedium,
     fontSize : 15,
     fontWeight : 'bold',
     lineHeight : 24,
     textAlign : 'center',
     letterSpacing : 1
    },
    headerText: {
      
    }
})
