import React from "react";
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    ActivityIndicator,
    Alert,
    TouchableOpacity,
    NetInfo,
    Image
} from "react-native";

import PropTypes from 'prop-types';

// import Image from 'react-native-remote-svg';
import  { SearchBar }  from 'react-native-elements'; 

import * as  Constants from '../common/Constants'
import * as APIHelper from '../helper/apiHelper' 

import EventEmitter from "react-native-eventemitter";
// import { TouchableOpacity } from "react-native-gesture-handler";

const limit = 20

export default class FavouriteRecipe extends React.Component {
   
    constructor() {
        super();
        this.state = {
            recipesList : [],
            offset : 0 ,
            isDataAvailable : true,
            isLoading : false,
            searchRecipe : '',
            searchListArr : [],
            recipesFound  : true,
            favouriteRecipe : false,
            searchBarVisible : false, 
        }
    }
    
    componentWillMount() {
        // api calling 
        EventEmitter.on("changeTabIndex1", (value)=>{
            this._getFavouriteRecipeList()
        });
        this._getFavouriteRecipeList()
    }

    componentDidMount() {
        EventEmitter.on("searchText", (value)=>{
            this.setState({
                searchRecipe : value,
                
            })
            setTimeout(() => {
                this._getFavouriteRecipeList()
            }, 100);
            
        });
        EventEmitter.on("onTapCancel", (value)=>{
            this.setState({
                searchRecipe : '',
                searchListArr : '',
                recipesFound : true,
                searchBarVisible : false

            })
            setTimeout(() => {
                this._getFavouriteRecipeList()
            }, 100);
            
        });
        EventEmitter.on("SearchBar", (value)=>{
            this.setState({
                searchBarVisible : true
            })    
        });
    }
    _getFavouriteRecipeList() {
       
        let offset = this.state.offset
        var searchRecipe = this.state.searchRecipe 
        // var url = this.state.searchRecipe == '' ?  APIHelper.baseURL + APIHelper.getFavouriteRecipe + '?CategoryId=&Language=&Difficulty=0&Name=&Offset=' + offset + '&Limit=' + limit + '&Portions=1' :  APIHelper.baseURL + APIHelper.getRecipe + '?CategoryId=&Language=&Difficulty=0&Name=' +searchRecipe + '&Offset=' + offset + '&Limit=' + limit + '&Portions=1' 
        var url = APIHelper.baseURL + APIHelper.getFavouriteRecipe
        this.setState({
            isLoading : true
        })
        
        APIHelper.getRequest(url, {
            'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
            "Content-Type": "application/json"

        }, (error, response) => {
            if(error === null & response === null) {
                this.setState({
                    isLoading : false
                })
            }
            else if(error != null) {
                this.setState({
                    isLoading : false
                })
                alert('Error ' + JSON.stringify(error));
            }
            else {
                this.getRecipeListesponse(response);
            }
        })
    }

    getRecipeListesponse = (response) => {
        this.setState({
            isLoading : false
        })
        if(response.Status == true) {
            this.setState({recipesFound : true})
            // check data length if grater zero then assign data to recipeList
            if(response.Data.length > 0) {
                if(this.state.searchRecipe == '') {
                    this.setState({
                        // recipesList: [...this.state.recipesList , ...response.Data],
                        recipesList : response.Data,
                        isDataAvailable: (response.Data.length < limit ) ? false : true
                    })
                }
                else {
                   
                    this.setState({
                        searchListArr  : response.Data,
                        isDataAvailable: (response.Data.length < limit ) ? false : true
                    })
                }
                
            }
            else {
                this.setState({
                    isDataAvailable : false
                })
            }
        }
        else {
            this.setState({recipesFound : false})
            //  alert(response.Message)
        }
    }

    // onTapRecipeCell = (item) => {
    //     // this.props.onReceipeTap(item);
    //     this.props.navigation.navigate('RecipeDetails', {recipeDetail : item.item , title : item.item.Name , isFavourite  : true});
    // }

    onTapRecipeCell = (item) => {
        // alert(JSON.stringify(item.item.Id))
        let url =  APIHelper.baseURL + APIHelper.getRecipe + item.item.Id            
        APIHelper.getRequest(url, {
            'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
            "Content-Type": "application/json"
        }, (error, response) => {
            if(error === null & response === null) {
                this.setState({
                    isLoading : false
                })
            }
            else if(error != null) {
                this.setState({
                    isLoading : false
                })
                // alert('Error ' + JSON.stringify(error));
            }
            else {
                this.getRecipeDetailResponse(response,item);
            }
        })    

        // this.props.onReceipeTap(item);
        // this.props.navigation.navigate('RecipeDetails', {recipeDetail : item.item , title : item.item.Name , isFavourite  : item.item.IsFavorites});
    }

    getRecipeDetailResponse = (response,item) => {
       
        if(response.Status == true) { 
            this.props.navigation.navigate('RecipeDetails', {title : response.Data.RecipeDetails.Title, isFavourite  : true, ownerName : item.item.OwnerName ,categoryName : item.item.CategoryName, recipeDetailsData : response.Data.RecipeDetails , Ingredents : response.Data.Ingredients , refreshList: () => { this.refreshList() }});   
        }
        else {
            console.log('recipe already favourite' + favouriterecipeId) 
        }
    }

    refreshList () {
        this.setState({
            recipesList : []
        })
        this._getFavouriteRecipeList()
    }


    renderItem = (item,index) => {
        let itemObj = item.item
        var difficulty=  itemObj.Difficulty == 0 ? "Lett" : itemObj.Difficulty == 1 ? "Middels" : "Vanskelig"
        var favouriteImage = itemObj.IsFavorites ? <Image style = {styles.favouriteImage}  source = {Constants.IC_FAVORITE_SELECTED} />: <Image style = {styles.favouriteImage}  source = {Constants.IC_FAVORITE_DESELECTED} />
        
        let hours =parseInt(itemObj.Duration) / 60
            let minutes =parseInt(itemObj.Duration) % 60
            var duration = ""
            if (parseInt(hours) > 0) {
                if (minutes > 0) {
                    duration = parseInt(hours) + " h" + parseInt(minutes) + ' min'
                    
                }
                else {
                    duration = parseInt(hours) + " h"                              
                }
            }
            else {
                duration = parseInt(minutes) + ' min'
                
            }
       
        return (
            <TouchableOpacity onPress = { () => {this.onTapRecipeCell(item)}} activeOpacity = {0.7}>
                <View style ={styles.itemContainer}>
                    <View style = {styles.recipeItem}>
                        <Image style = {styles.recipeImage}  source = {{uri : itemObj.Image}} />
                    
                        <Text style={styles.recipeName} allowFontScaling = {false}>{ itemObj.Name }</Text>
                        <Text style={styles.authorName} allowFontScaling = {false}>{ itemObj.OwnerName }</Text>
                        <View style={styles.durationView}>
                            <Text style = {styles.durationContent} allowFontScaling = {false}>{duration}</Text>
                            <View style = {{height : '90%',width:2,backgroundColor:'rgba(113,119,127,1)',margin : 10}}></View>
                            <Text style = {styles.durationContent} allowFontScaling = {false}>{itemObj.Portions + " porsjoner"}</Text>
                            <View style = {{height : '90%',width:2,margin : 10,backgroundColor:'rgba(113,119,127,1)'}}></View>
                            <Text style = {styles.durationContent} allowFontScaling = {false}>{difficulty}</Text>
                        </View>
                    </View> 
                    <TouchableOpacity onPress = {() => {this.removeFavourite(itemObj,item.index)}} style = {styles.favouriteTouchable} activeOpacity = {0.7}>
                        <Image style = {styles.favouriteImage}  source = {Constants.IC_FAVORITE_SELECTED} />
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
           
        )    
    }

    renderFooter = () => {
       
        var progressView = <View/> 

        if(this.state.isLoading) {

            progressView = <View style ={styles.renderFooter}>
                                <ActivityIndicator style ={styles.loading} color = 'red' size ='large'/>
                            </View>    
        }
        return (
            <View>
                {progressView}
            </View> 
        )
    }

    renderSeparator () {
        return (
            <View style ={{height :0 ,width :'100%',backgroundColor :'gray'}}> 
            </View>
        )
    }

    handleLoadMore = () => {
        // handle load more data incerement offsetvalue 
        if(this.state.isDataAvailable && !this.state.isLoading) {
            this.setState({
                offset : this.state.offset + limit
                }, ()=>{
                    this._getFavouriteRecipeList();
            })
        }
    }

    removeFavourite = (itemObj,index) => {
        
        var itemIndex  =  index
        var recipeId = itemObj.Id
        let url = APIHelper.baseURL + APIHelper.removeFavourite
        NetInfo.isConnected.fetch().then(isConnected =>{
            if(isConnected) {
                fetch(url,{
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-type': 'application/json',
                        'ApiKey':'0081fb62dc92699fcb5277e26c8588a07d7d6679af9339b739cd3e5d5654',
                    },
                    body: JSON.stringify({
                        UserId : 1,
                        RecipeId : recipeId
                    })
                    })
                    .then((response) => response.json())
                    // .then(json => this._handleResponse(json.response))
                    .then((responseJson) => {
                        this.removeFavouriteResponse(responseJson,recipeId,itemIndex);
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            }
        })
    }

    removeFavouriteResponse = (response,recipeId,itemIndex) => {
        var index = itemIndex
        if(response.Status == true) { 
            // EventEmitter.emit('unfavourite',recipeId)
            if(this.state.recipesList.length === 1){
                var recipeObj = this.state.recipesList
                recipeObj.splice(index , 1)
                this.setState({
                    recipesList:[...recipeObj],
                    recipesFound : false
                })  
              
            }
            else {
                // EventEmitter.emit('unfavourite',recipeId)
                var recipeObj = this.state.recipesList
                recipeObj.splice(index , 1)
                this.setState({
                    recipesList:[...recipeObj]
                })  
            }
              
        }
        else {
            console.log('recipe  favourite' + recipeId) 
        }
    }

    onTapBrowse = () => {
        this.props.navigation.setParams({
            hideSearchButton: false,
            hideFilterButton: false
          })
        EventEmitter.emit("BrowseTap")
    }

    onChangeSearchText = (text) =>{
        if(text === null){
            this.setState({
                searchRecipe : '',
                searchListArr : []
            })
            
            setTimeout(() => {
                this._getFavouriteRecipeList()
            }, 100);
        }
        else {
            this.setState({
                searchRecipe : text
            })
            
            setTimeout(() => {
                this._getFavouriteRecipeList()
            }, 300);
        }  
    }

    onTapCancelSearchText =() =>{
        this.setState({
            searchBarVisible : false,
            searchRecipe : '',
            searchListArr : []
        })
        setTimeout(() => {
            this._getFavouriteRecipeList()
        }, 300);
       EventEmitter.emit("onTapCancel")
    }

    onTapClearText =()=> {
        this.search.focus();
    }

    render() {
        var {recipesList} = this.state
        var progressView = <View/>
        var RecipeList
        // load data first time , dispaly indicator in center 
        var searchBarView = <View/>
        if(this.state.searchBarVisible){
            searchBarView =   
            <SearchBar
            ref={search => this.search = search}
            platform = "ios"//{(Platform.OS == 'ios')? "ios" : "android"}
            containerStyle = {{height : 50,width : Constants.deviceWidth-16,backgroundColor : 'white',alignSelf:'center',borderTopWidth : 0,borderBottomWidth:0,marginLeft:8,marginRight:8}}
            inputContainerStyle = {{height:'100%',backgroundColor : 'rgba(233,233,235,1)'}}
            placeholder="Søk etter navn eller ingrediens ..."
            onChangeText={(text) => this.onChangeSearchText(text)} 
            value={this.state.searchRecipe}
            onCancel = {()=> {this.onTapCancelSearchText()}}
            onClear = {()=> {this.onTapClearText()}}
            autoFocus = {true}
            allowFontScaling = {false}
            // onCancel = {navigation.getParam('onTapSearchCancel')}
          
          />
        }
        if(recipesList.length == 0 ){
            let animatingLoading = this.state.isLoading ? true : false
            progressView = <View style ={{height:'100%',width:'100%',marginTop:10,justifyContent:'center',alignItems:"center"}}>
                            <View style = {{backgroundColor : 'rgba(225,225,225,0.7)', borderRadius : 10 , height : 70 ,width : 70 , justifyContent : 'center' ,alignItems : 'center' }}>
                                <ActivityIndicator style ={styles.loading} color = 'red' size ='large' animating = {animatingLoading}/>
                            </View>
                        </View>
        }
        if(this.state.recipesFound){
            RecipeList = 
            <FlatList
                style = {styles.flatListStyle}
                showsVerticalScrollIndicator = {false}
                keyExtractor = { (item,index) => index.toString() }
                renderItem = {this.renderItem.bind(this)}
                data = {this.state.searchListArr == 0 ? this.state.recipesList : this.state.searchListArr }
                extraData = {this.state}
                ItemSeparatorComponent={this.renderSeparator}
                ListFooterComponent={this.renderFooter.bind(this)}
                // onEndReached = {this.handleLoadMore}
                // onEndReachedThreshold = {0.4}
             />
        }
        else {
            var RecipeList = 
            <View style = {styles.notFoundRecipe}>
                <Image source = {Constants.IC_NO_FAVOURITE_RECIPE_FOUND} style = {styles.recipeNotFoundImage}/>
                <Text style = {styles.notFoundRecipeText} numberOfLines = {2} allowFontScaling={false}>Du har ingen favorittoppskrifter</Text>
                <Text style = {styles.notFoundRecipeTextDescription} numberOfLines = {3} allowFontScaling={false}>Trykk på hjerte-ikonet i det høyre hjørnet av oppskriften for å legge den til i dine favoritter.</Text>
                <TouchableOpacity style = {styles.browseRecipe} activeOpacity ={0.7} onPress = { () => {this.onTapBrowse()}}> 
                    <Text style = {styles.browseRecipeText} allowFontScaling={false}>BLA GJENNOM OPPSKRIFTER</Text>
                </TouchableOpacity>
            </View>
        }
        return (
            <View style={styles.container}>
                {searchBarView}
                {RecipeList}
                {progressView}
            </View>
        );
    }
}

FavouriteRecipe.PropTypes = {
    onReceipeTap: PropTypes.func,
    navigation: PropTypes.any
}

FavouriteRecipe.defaultProps = {
    onReceipeTap: null,
    navigation: null
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
    },
    flatListStyle: {
        height :  Constants.deviceHeight,
        width : Constants.deviceWidth,
    },
    itemContainer: {
        height : 336,
        width : Constants.deviceWidth,
        flexDirection : 'column',
    },
    recipeItem: {
        height : 336,
        width : Constants.deviceWidth,
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center'
    },
    recipeImage: {
        height : 224,
        width : Constants.deviceWidth-24,
        borderRadius : 4,
    },
    recipeContent: {
        height : 90, 
        flex: 1,
        marginRight : 3,
        marginLeft : 4,
        justifyContent : 'center'
    },
    recipeContentText: {
        height : 20,
        fontSize : 15,
        marginRight : 10    
    },
    loading: {
        left : 0,
        right : 0,
        top : 0,
        bottom : 0,
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : 'rgba(0,0,0,0)'
    },
    renderFooter: {
        height : '10%',
        width : '100%',
        marginTop : 10,
        justifyContent : 'center',
        alignItems : 'center',    
    },
    recipeName: {
        fontSize : 18, 
        lineHeight : 24, 
        fontWeight : 'bold', 
        width : Constants.deviceWidth-24, 
        marginTop : '2%',
        color : Constants.RECIPE_NAME_COLOR ,
        fontFamily : Constants.RobotoMedium,
    },
    authorName: {
        fontSize : 14, 
        lineHeight : 20, 
        fontWeight: '500',
        width : Constants.deviceWidth-24, 
        marginTop : 3,
        height : 22, 
        color : Constants.RECIPE_AUTHOR_NAME_COLOR,
        fontFamily : Constants.RobotoMedium,
    },
    durationView: {
        width : Constants.deviceWidth-24,
        height : '5%',
        flexDirection : 'row',
        alignItems : 'center',
        marginTop : 4
    },
    durationContent: {
        height: 20 , 
        color: 'rgba(113,119,127,1)', 
        fontSize: 14, 
        lineHeight: 20,
        textAlign:'left',
        fontFamily : Constants.RobotoRegular,
    },
    notFoundRecipe: {
        height : '100%' ,
        width : '100%', 
        justifyContent : 'center',
        alignItems : 'center'
    },
    notFoundRecipeText: {
        width: Constants.deviceWidth - 48,
        color: 'rgba(28,40,51,1)',
        fontSize: 24,
        fontWeight: 'bold',
        lineHeight: 32,
        textAlign: 'center',
        fontFamily : Constants.RobotoMedium,
    },
    notFoundRecipeTextDescription : {
        width: Constants.deviceWidth - 48,
        color: 'rgba(62,72,82,1)',        
        fontSize: 16,
        lineHeight: 24,
        textAlign: 'center', 
        marginTop : 12 ,
        fontFamily : Constants.RobotoRegular,     
    },
    recipeNotFoundImage: {
        height : 50,
        width : 50,
        resizeMode  :'contain'
    },
    favouriteImage: {
        height : 30,
        width : 33,
    },
    favouriteTouchable: {
        height : Constants.deviceWidth * 0.11 , 
        width : Constants.deviceWidth * 0.11, 
        marginLeft: Constants.deviceWidth - 60, 
        marginTop: 25, 
        backgroundColor : 'white',
        borderRadius :(Constants.deviceWidth * 0.11) / 2 , 
        position : 'absolute' ,
        alignItems : 'center',
        justifyContent : 'center' 
    },
    browseRecipe: {
        height: 48,
        width: Constants.deviceWidth -48,
        borderRadius: 4,
        backgroundColor: 'rgba(243,83,83,1)',
        justifyContent  :'center',
        alignItems : 'center',
        marginTop  : 40
    },
    browseRecipeText: {
        height: 24,
        color: 'rgba(255,255,255,1)',
        fontSize: 16,
        fontWeight: 'bold',
        letterSpacing: 1,
        lineHeight: 24,
        textAlign: 'center',
        fontFamily : Constants.RobotoMedium,
    } 
    
})